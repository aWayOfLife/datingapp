using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Data
{
    public class Seed
    {
        public static async Task SeedAsync(DataContext context, ILoggerFactory loggerFactory)
        {
            try
            {
                if (!await context.Users.AnyAsync())
                {
                    var usersData = File.ReadAllText("../Infrastructure/Data/SeedData/UserSeedData.json");
                    if(usersData==null) return;
                    var users = JsonSerializer.Deserialize<List<AppUser>>(usersData);
                    foreach (var user in users)
                    {
                        using var hmac = new HMACSHA512();
                        user.Username = user.Username.ToLower();
                        user.PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes("Pa$$w0rd"));
                        user.PasswordSalt = hmac.Key;
                        context.Users.Add(user);
                        
                    }
                    await context.SaveChangesAsync();
                }
               
            }
            catch (System.Exception ex)
            {
                var logger = loggerFactory.CreateLogger<Seed>();
                logger.LogError(ex.Message);
                logger.LogError(ex.StackTrace);
            }
        }
    }

    
}